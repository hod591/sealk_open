# README #

This project represent a technical test for the hiring process with SEALK. 
 
### The work done
I chose to do this test with Python which is the language that I am more fluent with, so all the modification has been made on the repository sealk_open/src/main/python/assessment with the script main.py.
 
The job done is to make a parallel processing for computing the top companies based on score that is given by a predefined function. I used a thread pool with a default number of threads equal to the number of cores of the machine. But, I also left the choice to the user to define the number of threads. The same is the case for the number of top companies to extract, and the file to use. I chose 10 as a default number, and a file that I stored in an internal repository as default value of the filename.
 
### Manual to use: 
To execute the script, you can go the repository of the script and then write on your terminal the following command: 
 
python main.py
 
This command allows you to execute and show the result using the file nasdaq-company-list.csv which is stored on the repository data. 
 
If you want to choose another file, the number of threads or the number of top companies to show, you can use options, you will have the following manual by executing python main.py --help :

Usage: main.py [OPTIONS]

  Args:     filename (str): Input CSV file path.     top (int): Number of
  companies to print at the end. Defaults to 10.     threads (int): Number of
  threads to use in calculating. Defaults to the number of CPU.

Options:
  --filename TEXT    Input CSV file name
  --top INTEGER      Number of companies Symbols to print at the end
  --threads INTEGER  Number of threads to use
  --help             Show this message and exit.

