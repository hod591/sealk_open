# System imports
import sys
from io import StringIO

# Packages imports:
import click
import logging
import multiprocessing as mp
from os import path, pardir
import pandas as pd

# Local imports
sys.path.append(path.abspath(path.join(path.dirname(__file__), pardir)))
from common.dummy_ai import getCompanyAttractiveness

# Logger definition
logging.basicConfig(format="%(message)s", level=logging.INFO)

@click.command()
@click.option(
    "--filename",
    default="../../../../data/nasdaq-company-list.csv",
    help="Input CSV file name"
)
@click.option(
    "--top" ,
    default=10,
    help="Number of companies Symbols to print at the end"
)
@click.option(
    "--threads",
    default=mp.cpu_count(),
    help="Number of threads to use"
)
def main(filename: str, top:int, threads:int) -> None:
    """
    Args:
        filename (str): Input CSV file path.
        top (int): Number of companies to print at the end. Defaults to 10.
        threads (int): Number of threads to use in calculating. Defaults to the number of CPU.
    """
    # Parameters:
    # Create a thread pool basing on the number of cores: 
    pool = mp.Pool(threads)
    # Create the path to save the result:
    path = r'../../../../data'
    savefilename = r'/top{}companies.csv'.format(top)

    # Load data
    companies = pd.read_csv(filename)[:8].set_index('Symbol')
    # Get the ids of each company
    companies_ids = companies.index

    # Start the processing using the thread pool
    result = pool.map(getCompanyAttractiveness,companies_ids)

    # Close the thread pool
    pool.close()

    # Sort the result to get top's companies
    top_ids = [company['id'] for company in sorted(result,key=lambda company: company['score'],reverse=True)[:top]]
    # Get the informations of the top's companies
    top = companies.loc[top_ids]

    # Print and save the data
    logging.info(top)
    top.to_csv(path+savefilename, index=True, sep='|')


if __name__ == "__main__":
    main()
    